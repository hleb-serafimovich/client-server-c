
#pragma comment (lib,"Ws2_32.lib")
#pragma warning(disable: 4996)


#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring> 
#include<iostream>
#include <vector>
#include <fstream>
#include <conio.h>
#include <iomanip>
#include <dos.h>

using namespace std;



int numcl = 0;
string INFO_FILE = "C:/dd/Info.txt"; 
vector<string> infoVector;
string infoStr;
int counter = 0;



DWORD WINAPI ThreadFunc(LPVOID client_socket);
void sendInfoArray(LPVOID client_socket);
void addInfoInArray(LPVOID client_socket);
void DeleteInfo(LPVOID client_socket);
void updateInfo(LPVOID client_socket);
void readFileInfo(string fileName);
void writeFileInfo();
void print();
string updateStr();






void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	WSAData wsData;
	WORD DLLVersion = MAKEWORD(2, 1);   
	if (WSAStartup(DLLVersion, &wsData) != 0) 
	{
		cout << "Error!" << endl;
		system("pause");
		exit(1);
	}

	SOCKADDR_IN addr;  
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1111);

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, 0);
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr));
	listen(sListen, 5);

	SOCKET client_socket;    
	SOCKADDR_IN client_addr; 
	int client_addr_size = sizeof(client_addr);

	while (client_socket = accept(sListen, (sockaddr *)&client_addr, &client_addr_size))
	{
		counter++;
		print();
		cout << "New client\nport - " << ntohs(client_addr.sin_port) << "\ndeskriptor:" << client_socket << "\nIP: " << inet_ntoa(client_addr.sin_addr) << endl;
		DWORD thID;  
		CreateThread(NULL, NULL, ThreadFunc, &client_socket, NULL, &thID);
	}

}


void print()
{
	system("cls");
	cout << "Server receive ready" << endl;
	cout << "clients on the server: " << counter << endl;
}


DWORD WINAPI ThreadFunc(LPVOID client_socket)
{

	SOCKET s2 = ((SOCKET *)client_socket)[0];
	char choice[2];
	readFileInfo(INFO_FILE);
	sendInfoArray(&s2);

	while (recv(s2, choice, sizeof(choice), 0))
	{

		switch (choice[0])
		{
		case '2':  addInfoInArray(client_socket); break;
		case '3':  updateInfo(client_socket); break;
		case '4':  DeleteInfo(client_socket); break;
		}

	}

	counter--;
	print();
	closesocket(s2);
	return 0;
}






void readFileInfo(string fileName)
{

	int i = 0;
	string temp;
	infoVector.clear();
	ifstream read(fileName, ios::in);
	if (!read.is_open())
	{
		cout << "\n\t\tThe specified file does not exist!" << endl;
	}
	else
	{
		while (!read.eof())
		{
			getline(read, temp);
			if (temp == "")
			{
				continue;
			}
			infoVector.push_back(temp);
			infoStr += infoVector[i];
			infoStr += '\n';
			i++;
		}
	}

	read.close();

}


void writeFileInfo()
{
	infoStr = updateStr();
	ofstream write(INFO_FILE, ios::out);  
	write << infoStr;
	write.close();

}





void sendInfoArray(LPVOID client_socket)
{

	SOCKET s2 = ((SOCKET *)client_socket)[0];
	int msgSize = infoVector.size();
	send(s2, (char*)&msgSize, sizeof(int), NULL);
	for (int i = 0; i < infoVector.size(); i++)
	{
		send(s2, infoVector.at(i).c_str(), infoVector.at(i).length(), NULL);
		Sleep(10);
	}
	writeFileInfo();
}


void addInfoInArray(LPVOID client_socket)
{
	SOCKET s2 = ((SOCKET *)client_socket)[0];
	char msg[256];
	int rv = recv(s2, msg, sizeof(msg), NULL);
	msg[rv] = '\0';
	infoVector.push_back(msg);
	writeFileInfo();
}


void DeleteInfo(LPVOID client_socket)
{
	SOCKET s2 = ((SOCKET *)client_socket)[0];
	char RecordNumber[256];
	int rv = recv(s2, RecordNumber, sizeof(RecordNumber), NULL);

	RecordNumber[rv] = '\0';
	if (infoVector[0] != "" && atoi(RecordNumber) != 0 && atoi(RecordNumber) <= infoVector.size())
	{
		infoVector.erase(infoVector.begin() + atoi(RecordNumber) - 1);
	}
	writeFileInfo();
}


void updateInfo(LPVOID client_socket)
{
	SOCKET s2 = ((SOCKET *)client_socket)[0];
	char RecordNumber[256];
	char updateString[256];

	int rv = recv(s2, RecordNumber, sizeof(RecordNumber), NULL);
	RecordNumber[rv] = '\0';
	rv = recv(s2, updateString, sizeof(updateString), NULL);
	updateString[rv] = '\0';

	if (infoVector[0] != "" && atoi(RecordNumber) != 0 && atoi(RecordNumber) <= infoVector.size())
	{
		infoVector[atoi(RecordNumber) - 1] = updateString;
	}
	writeFileInfo();
}


string updateStr()
{
	int i = 0;
	string tempInfoStr;

	while (i < infoVector.size())
	{
		tempInfoStr += infoVector[i];
		tempInfoStr += '\n';
		i++;
	}


	return tempInfoStr;
}


