

#pragma comment (lib,"Ws2_32.lib")
#pragma warning(disable: 4996)


#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring> 
#include<iostream>
#include <fstream>
#include <conio.h>
#include <iomanip>
#include <vector>

using namespace std;


void ProgramRealization();   
void ShowInfoArray();    
void SendNewInfo(LPVOID client_socket);       
void DeleteInfo(LPVOID client_socket);
void updateInfo(LPVOID client_socket);
string concatenation(string first, string second);
string dataInput();
vector<string> infoVector;





void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	ProgramRealization();
}



void ProgramRealization()
{
	int choice;
	bool flag = true;
	bool exit_flag = true;
	char buf[100], *vec, tempVec[100];
	int VectorSize;
	string st;

	WSAData wsData;
	WORD DLLVersion = MAKEWORD(2, 1);  
	if (WSAStartup(DLLVersion, &wsData) != 0) 
	{
		cout << "Error!" << endl;
		system("pause");
		exit(1);
	}

	SOCKADDR_IN addr;  
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1111);
	SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
	if (connect(s, (SOCKADDR *)&addr, sizeof(addr)) != 0)
	{
		cout << "Error" << endl;
		system("pause");
		exit(1);
	}


	recv(s, (char*)&VectorSize, sizeof(int), NULL);
	for (int i = 0; i < VectorSize; i++)
	{
		int rv = recv(s, tempVec, sizeof(tempVec), NULL);
		vec = new char[strlen(tempVec) + 1];
		vec = tempVec;
		vec[rv] = '\0';
		st = vec;
		infoVector.push_back(st);
	}


	while (exit_flag)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t|" << "\t\t\t\t\t  M A I N   M E N U" << "\t\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |  V I E W   R E C O R D S" << "\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |  A D D   R E C O R D" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  3  |   E D I T V N G   R E C O R D" << "\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  4  |   D E L E T E   R E C O R D" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "         E X I T \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();


		switch (choice)
		{
		case '1': ShowInfoArray(); break;
		case '2': SendNewInfo((void*)s); break;   
		case '3': updateInfo(&s); break;
		case '4': DeleteInfo(&s); break;
		case  27: exit_flag = false; break;
		}

	}

	closesocket(s);
	WSACleanup();
}



void ShowInfoArray()
{
	system("cls");
	char *msg = nullptr;
	char *ptr = nullptr;

	if (infoVector.size() == 0)
	{
		cout << "\n\t\t List is empty!\n";
		system("pause");
		return;
	}
	cout << setw(18) << left << " � " << setw(18) << left << "|     Author      | " << setw(18) << left << "       Initials      | " << setw(18) << left << "      Title of the book     | " << setw(18) << left << "   The year of publishing     | " << setw(18) << left << "        Publisher      |" << setw(18) << left << "     Number of pages       | " << endl;
	for (int i = 0; i < infoVector.size(); i++)
	{
		ptr = new char[infoVector.at(i).length() + 1];
		strcpy(ptr, infoVector.at(i).c_str());
		msg = strtok(ptr, " ");
		do
		{
			cout << setw(20) << left << msg << '\t';
		} while (msg = strtok(0, " "));
	}

	system("pause");
}

void SendNewInfo(LPVOID client_socket)
{
	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t       A D D   R E C O R D" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;

	char choice[2] = "2";
	send(((SOCKET *)client_socket)[0], choice, strlen(choice), NULL);
	string newInfo;

	newInfo = dataInput();
	send(((SOCKET *)client_socket)[0], newInfo.c_str(), newInfo.size(), NULL);
	infoVector.push_back(newInfo);
	system("pause");

}


string concatenation(string first, string second)
{
	first += second + ' ' + ' ';
	return first;
}

string dataInput()
{
	string newInfo;
	string tempInfo;

	cout << "\n\t\tEnter the registration number of the book: ";
	getline(cin, tempInfo);
	newInfo = concatenation(newInfo, tempInfo);
	cout << "\n\t\tEnter the author of the book: ";
	getline(cin, tempInfo);
	newInfo = concatenation(newInfo, tempInfo);
	cout << "\n\t\tEnter the name of the book: ";
	getline(cin, tempInfo);
	newInfo = concatenation(newInfo, tempInfo);
	cout << "\n\t\tEnter the year of publication: ";
	getline(cin, tempInfo);
	newInfo = concatenation(newInfo, tempInfo);
	cout << "\n\t\tEnter Publisher: ";
	getline(cin, tempInfo);
	newInfo = concatenation(newInfo, tempInfo);
	cout << "\n\t\tEnter the number of pages: ";
	getline(cin, tempInfo);
	newInfo += tempInfo;

	return newInfo;
}



void DeleteInfo(LPVOID client_socket)
{
	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t\t        D E L E T E   R E C O R D" << "\t\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;

	char choice[2] = "4";
	string RecordNumber;
	send(((SOCKET *)client_socket)[0], choice, strlen(choice), NULL);

	cout << "\n\t\tEnter the record number: ";
	getline(cin, RecordNumber);
	send(((SOCKET *)client_socket)[0], RecordNumber.c_str(), RecordNumber.size(), NULL);
	if (infoVector[0] != "" && atoi(RecordNumber.c_str()) != 0 && atoi(RecordNumber.c_str()) <= infoVector.size())
	{
		infoVector.erase(infoVector.begin() + atoi(RecordNumber.c_str()) - 1);
	}

}


void updateInfo(LPVOID client_socket)
{
	system("cls");
	cout << "  \n\t\t ________________________________________________________________________________________________________" << endl;
	cout << "  \t\t| \t\t\t       E D I T V N G   R E C O R D" << "\t\t\t\t |" << endl;
	cout << "  \t\t|________________________________________________________________________________________________________|" << endl;

	char choice[2] = "3";
	string newInfo;
	string RecordNumber;
	send(((SOCKET *)client_socket)[0], choice, strlen(choice), NULL);

	cout << "\n\t\tEnter the record number:: ";
	getline(cin, RecordNumber);
	send(((SOCKET *)client_socket)[0], RecordNumber.c_str(), RecordNumber.size(), NULL);
	newInfo = dataInput();
	send(((SOCKET *)client_socket)[0], newInfo.c_str(), newInfo.size(), NULL);

	if (infoVector[0] != "" && atoi(RecordNumber.c_str()) != 0 && atoi(RecordNumber.c_str()) <= infoVector.size())
	{
		infoVector[atoi(RecordNumber.c_str()) - 1] = newInfo;
	}


}


